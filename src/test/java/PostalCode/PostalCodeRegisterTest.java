package PostalCode;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PostalCodeRegisterTest {

    PostalCodeRegister postalCodeRegister;
    PostalCode postalCode;
    PostalCode postalCode1;
    PostalCode postalCode2;
    PostalCode postalCode3;
    PostalCode postalCode4;

    @BeforeEach
    @DisplayName("Making test values")
    void createTestValues() {
        postalCodeRegister = new PostalCodeRegister();
        postalCode = new PostalCode("1250","OSLO","3010","OSLO",'G');
        postalCode1 = new PostalCode("1251","OSLO","3001","OSLO",'G');
        postalCode2 = new PostalCode("1300","SANDVIKA","3024","BÆRUM",'P');
        postalCode3 = new PostalCode("2801","GJØVIK","3407","GJØVIK",'P');
        postalCode4 = new PostalCode("2807","GJØSTAD","3407","GJØVIK",'P');

        postalCodeRegister.addPostalCode(postalCode);
        postalCodeRegister.addPostalCode(postalCode1);
        postalCodeRegister.addPostalCode(postalCode2);
        postalCodeRegister.addPostalCode(postalCode3);
        postalCodeRegister.addPostalCode(postalCode4);
    }

    // Dont create negative tests on addPostalCode, since i test setters in postal code, which essentially end up being the same thing.
    @Test
    @DisplayName("Add postal code")
    void addPostalCode() {
        PostalCode postalCode5 = new PostalCode("2801","GJØVIK","3402","GJØVIK",'S');
        PostalCodeRegister postalCodeRegister = new PostalCodeRegister();
        postalCodeRegister.addPostalCode(postalCode5);
        assertTrue(postalCodeRegister.getPostalCodes().contains(postalCode5));
    }

    @Test
    @DisplayName("Full search for postal code")
    void searchPostalCode() {
        int length = postalCodeRegister.searchPostalCode("2801").size();
        assertEquals(1,length);
    }

    @Test
    @DisplayName("Empty search for postal code")
    void searchEmptyPostalCode() {
        int length = postalCodeRegister.searchPostalCode("").size();
        assertEquals(postalCodeRegister.getPostalCodes().size(),length);
    }

    @Test
    @DisplayName("Partial search for postal code")
    void searchPartialPostalCode() {
        int length = postalCodeRegister.searchPostalCode("28").size();
        assertEquals(2,length);
    }

    @Test
    @DisplayName("Search for postal code not in list")
    void searchPostalCodeNotInList() {
        int length = postalCodeRegister.searchPostalCode("9999").size();
        assertEquals(0,length);
    }

    @Test
    @DisplayName("Null value search for postal code - Throws")
    void searchNullPostalCode() {
        assertThrows(NullPointerException.class,() -> {postalCodeRegister.searchPostalCode(null);});
    }


    @Test
    @DisplayName("Full search for city")
    void searchCity() {
        int length = postalCodeRegister.searchCity("GJØVIK").size();
        assertEquals(1,length);
    }

    @Test
    @DisplayName("Empty search for city")
    void searchEmptyCity() {
        int length = postalCodeRegister.searchCity("").size();
        assertEquals(postalCodeRegister.getPostalCodes().size(),length);
    }

    @Test
    @DisplayName("Partial search for city")
    void searchPartialCity() {
        int length = postalCodeRegister.searchCity("GJØ").size();
        assertEquals(2,length);
    }

    @Test
    @DisplayName("Search for city not in list")
    void searchCityNotInList() {
        int length = postalCodeRegister.searchCity("LEIFJORD").size();
        assertEquals(0,length);
    }

    @Test
    @DisplayName("Null value search for city - Throws")
    void searchNullCity() {
        assertThrows(NullPointerException.class,() -> {postalCodeRegister.searchCity(null);});
    }


    @Test
    @DisplayName("Full search for both")
    void searchBoth() {
        int length = postalCodeRegister.searchBoth("1251","OSLO").size();
        assertEquals(1,length);
    }

    @Test
    @DisplayName("Empty search for both")
    void searchBothEmpty() {
        int length = postalCodeRegister.searchBoth("","").size();
        assertEquals(postalCodeRegister.getPostalCodes().size(),length);
    }

    @Test
    @DisplayName("Partial search for both")
    void searchPartialBoth() {
        int length = postalCodeRegister.searchBoth("280","GJØ").size();
        assertEquals(2,length);
    }

    @Test
    @DisplayName("Search for both not in list")
    void searchBothNotInList() {
        int length = postalCodeRegister.searchBoth("9999","LEIFJORD").size();
        assertEquals(0,length);
    }

    @Test
    @DisplayName("Search for both with one not in list")
    void searchBothOneNotInList() {
        int length = postalCodeRegister.searchBoth("9999","OSLO").size();
        assertEquals(0,length);
    }

    @Test
    @DisplayName("Null value search for both - Throws")
    void searchBothNull() {
        assertThrows(NullPointerException.class,() -> {postalCodeRegister.searchBoth(null,null);});
    }

    @Test
    @DisplayName("Get Postal codes")
    void getPostalCodes() {
        List<PostalCode> list = postalCodeRegister.getPostalCodes();
        assertTrue((list.contains(postalCode)&list.contains(postalCode1)
                &list.contains(postalCode2)&list.contains(postalCode3)&list.contains(postalCode4)));
    }
}