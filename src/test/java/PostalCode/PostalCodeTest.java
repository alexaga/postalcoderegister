package PostalCode;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PostalCodeTest {

    private PostalCode postalCode;

    @BeforeEach
    @DisplayName("Creating test values")
    void createTestValues() {
        postalCode = new PostalCode("1250","OSLO","3010","OSLO",'G');
    }

    // setPostalCode and setMunicipalityCode is set up identically, therefore i only test one of them
    @Test
    @DisplayName("Set postal code")
    void setPostalCode() {
        postalCode.setPostalCode("0000");
        assertEquals("0000",postalCode.getPostalCode());
    }

    @Test
    @DisplayName("Set empty postal code")
    void setEmptyPostalCode() {
        assertThrows(IllegalArgumentException.class, () -> {postalCode.setPostalCode("");});
    }

    @Test
    @DisplayName("Set too short postal code")
    void setTooShortPostalCode() {
        assertThrows(IllegalArgumentException.class, () -> {postalCode.setPostalCode("00");});
    }

    @Test
    @DisplayName("Set too long postal code")
    void setTooLongPostalCode() {
        assertThrows(IllegalArgumentException.class, () -> {postalCode.setPostalCode("000000");});
    }

    @Test
    @DisplayName("Set Null postal code")
    void setNullPostalCode() {
        assertThrows(NullPointerException.class, () -> {postalCode.setPostalCode(null);});
    }

    // setCity and setMunicipality is set up identically, therefore i only test one of them
    @Test
    @DisplayName("Set city")
    void setCity() {
        postalCode.setCity("GJØVIK");
        assertEquals("GJØVIK",postalCode.getCity());
    }

    @Test
    @DisplayName("Set empty city")
    void setEmptyCity() {
        assertThrows(IllegalArgumentException.class, () -> {postalCode.setCity("");});
    }

    @Test
    @DisplayName("Set Null city")
    void setNullCity() {
        assertThrows(NullPointerException.class, () -> {postalCode.setCity(null);});
    }

    // setCategory tests
    @Test
    @DisplayName("Set category")
    void setCategory() {
        postalCode.setCategory('B');
        assertEquals('B',postalCode.getCategory());
    }

    @Test
    @DisplayName("Set illegal character for category")
    void setIllegalCharCategory() {
        assertThrows(IllegalArgumentException.class, () -> {postalCode.setCategory('A');});
    }
}