package Utility;

import PostalCode.PostalCodeRegister;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ImportClassTest {

    PostalCodeRegister postalCodeRegister;

    @BeforeEach
    @DisplayName("Creating test values")
    void createTestValues() {
        postalCodeRegister = new PostalCodeRegister();
    }

    @Test
    @DisplayName("Import postal codes to postal code register")
    void importPostalCodes() {
        ImportClass.importPostalCodes(postalCodeRegister);
        assertEquals(5132,postalCodeRegister.getPostalCodes().size());
    }

    @Test
    @DisplayName("Import postal codes to null-register")
    void importNullPostalCodes() {
        assertThrows(NullPointerException.class,() -> {ImportClass.importPostalCodes(null);});
    }
}