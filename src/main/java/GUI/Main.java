package GUI;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.apache.log4j.Logger;
import java.io.IOException;

/**
 * The type Main.
 */
// Standard Class setup from archetypes
public class Main extends Application {

    private static Scene scene;

    private static final Logger LOGGER = Logger.getLogger(Main.class.getName());

    @Override
    public void start(Stage stage) throws IOException {
        LOGGER.info("Initializing main stage from "+Main.class.getName());
        scene = new Scene(loadFXML("/View/Main"));
        stage.setTitle("Postal Code Register");
        stage.setMinHeight(400);
        stage.setMinWidth(610);
        // Added free icon from freeicons.io
        stage.getIcons().add(new Image(Main.class.getResourceAsStream("/Images/icon.png")));
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Sets root.
     *
     * @param fxml the fxml path
     * @throws IOException IOException
     */
    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) { launch(); }
}