package Controller;

import PostalCode.PostalCode;
import PostalCode.PostalCodeRegister;
import Utility.AlertClass;
import Utility.ImportClass;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import org.apache.log4j.Logger;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

/**
 * The type Controller main.
 */
public class ControllerMain implements Initializable {

    private static final Logger LOGGER = Logger.getLogger(ControllerMain.class.getName());

    private static PostalCodeRegister postalCodeRegister;
    private ObservableList<PostalCode> postalCodeListWrapper;

    /**
     * The Postal code table view.
     */
    public TableView<PostalCode> postalCodeTableView;
    /**
     * The Postal Code search field.
     */
    public TextField pCSearch;
    /**
     * The City search field.
     */
    public TextField cSearch;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        LOGGER.info("Initializing project from "+ControllerMain.class.getName());
        postalCodeRegister = new PostalCodeRegister();
        ImportPostalCodes();
        setUpTable();
    }

    private void ImportPostalCodes() {
        try {
            ImportClass.importPostalCodes(postalCodeRegister);
        } catch (NullPointerException npe) {
            LOGGER.fatal("Could not import postal codes. "+npe.getMessage());
        }
    }

    /**
     * Postal code search.
     */
    public void postalCodeSearch() {
        try {
            postalCodeListWrapper.setAll(this.postalCodeRegister.searchPostalCode(pCSearch.getText()));
        } catch (NullPointerException ne) {
            LOGGER.warn("Nullpointer value entered. "+ne.getMessage());
        }
    }

    /**
     * City search.
     */
    public void citySearch() {
        try {
            postalCodeListWrapper.setAll(this.postalCodeRegister.searchCity(cSearch.getText()));
        } catch (NullPointerException ne) {
            LOGGER.warn("Nullpointer value entered. "+ne.getMessage());
        }
    }

    /**
     * Search for both city and postal code fields.
     */
    public void searchBoth() {
        try {
            postalCodeListWrapper.setAll(this.postalCodeRegister.searchBoth(pCSearch.getText(),cSearch.getText()));
        } catch (NullPointerException ne) {
            LOGGER.warn("Nullpointer value entered. "+ne.getMessage());
        }
    }

    /**
     * Sets up table.
     */
    public void setUpTable() {
        LOGGER.info("Setting up tables");
        // Creating all columns
        TableColumn<PostalCode, String> postalCodeCol = new TableColumn<>("Postal Code");
        postalCodeCol.setCellValueFactory(new PropertyValueFactory<PostalCode, String>("postalCode"));
        postalCodeCol.setReorderable(false);

        TableColumn<PostalCode, String> cityCol = new TableColumn<>("City");
        cityCol.setCellValueFactory(new PropertyValueFactory<PostalCode, String>("city"));
        cityCol.setReorderable(false);

        TableColumn<PostalCode, String> municipalityCodeCol = new TableColumn<>("Municipality Code");
        municipalityCodeCol.setCellValueFactory(new PropertyValueFactory<PostalCode, String>("municipalityCode"));
        municipalityCodeCol.setReorderable(false);

        TableColumn<PostalCode, String> municipalityCol = new TableColumn<>("Municipality");
        municipalityCol.setCellValueFactory(new PropertyValueFactory<PostalCode, String>("municipality"));
        municipalityCol.setReorderable(false);

        TableColumn<PostalCode, Character> categoryCol = new TableColumn<>("Category");
        categoryCol.setCellValueFactory(new PropertyValueFactory<PostalCode, Character>("category"));
        categoryCol.setReorderable(false);

        postalCodeTableView.setItems(getPostalCodeListWrapper());
        postalCodeTableView.getColumns().addAll(postalCodeCol, cityCol, municipalityCodeCol, municipalityCol, categoryCol);
        postalCodeTableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
    }

    /**
     * Gets the observablelist displayed in tableview
     * @return patientListWrapper
     */
    //Code inspired by javafx 17 example project on blackboard
    private ObservableList<PostalCode> getPostalCodeListWrapper() {
        postalCodeListWrapper = FXCollections.observableArrayList(this.postalCodeRegister.getPostalCodes());
        return postalCodeListWrapper;
    }

    /**
     * Updates observable list displayed in tableview.
     */
//Code inspired by javafx 17 example project on blackboard
    public void updateObservableList() {
        postalCodeListWrapper.setAll(this.postalCodeRegister.getPostalCodes());
    }

    /**
     * Exit the application.
     */
    public void exit() {
        if(AlertClass.createConfirmationAlert("Are you sure you want to exit",
                "If so, click OK")) {
            LOGGER.info("Exiting application");
            System.exit(0);
        }
    }

    /**
     * opens About window.
     */
    public void about() {
        AlertClass.createInformationAlert("Postal Code Register\nVersion 1.0",
                "Application Created By\nAlexander Gatland\n"+ LocalDate.now());
    }

    /**
     * opens Category info window
     */
    public void categoryInfo() {
        AlertClass.createInformationAlert("Category descriptions",
                "B = Both street addresses and mailboxes\n" +
                        "F = Multiple applications (common)\n" +
                        "G = Street addresses (and place addresses)\n" +
                        "P = Mailboxes\n" +
                        "S = Service postal codes");
    }
}