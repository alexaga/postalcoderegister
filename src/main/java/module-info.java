module PostalCodeRegister {
    requires javafx.graphics;
    requires javafx.controls;
    requires javafx.fxml;
    requires log4j;
    exports  GUI;
    exports Controller;
    opens Utility;
    opens PostalCode;
}