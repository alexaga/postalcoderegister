package PostalCode;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * The type Postal code.
 */
public class PostalCode {
    private String postalCode;
    private String city;
    private String municipalityCode;
    private String municipality;
    private char category;

    // Inspired by thread on stackoverflow
    private static final Set<Character> categories = new HashSet<Character>(Arrays.asList('B', 'F', 'G','P','S'));

    /**
     * Instantiates a new Postal code.
     *
     * @param postalCode       the postal code
     * @param city             the city
     * @param municipalityCode the municipality code
     * @param municipality     the municipality
     * @param category         the category
     */
    public PostalCode(String postalCode, String city, String municipalityCode, String municipality, char category) {
        setPostalCode(postalCode);
        setCity(city);
        setMunicipalityCode(municipalityCode);
        setMunicipality(municipality);
        setCategory(category);
    }

    /**
     * Sets the postal code.
     *
     * @param postalCode the postal code
     */
    public void setPostalCode(String postalCode) {
        if (postalCode == null) {
            throw new NullPointerException("Postal code can not be empty");
        } else if (postalCode.isBlank()) {
            throw new IllegalArgumentException("Postal code can not be blank");
        } else if (postalCode.length() != 4) {
            throw new IllegalArgumentException("Postal code length must be 4");
        }
        this.postalCode = postalCode;
    }

    /**
     * Sets the city.
     *
     * @param city the city
     */
    public void setCity(String city) {
        if (city == null) {
            throw new NullPointerException("City can not be empty");
        } else if (city.isBlank()) {
            throw new IllegalArgumentException("City can not be blank");
        }
        this.city = city;
    }

    /**
     * Sets the municipality code.
     *
     * @param municipalityCode the municipality code
     */
    public void setMunicipalityCode(String municipalityCode) {
        if (municipalityCode == null) {
            throw new NullPointerException("MunicipalityCode can not be empty");
        } else if (municipalityCode.isBlank()) {
            throw new IllegalArgumentException("MunicipalityCode can not be blank");
        } else if (municipalityCode.length() != 4) {
            throw new IllegalArgumentException("Municipality code length must be 4");
        }
        this.municipalityCode = municipalityCode;
    }

    /**
     * Sets the municipality.
     *
     * @param municipality the municipality
     */
    public void setMunicipality(String municipality) {
        if (municipality == null) {
            throw new NullPointerException("Municipality can not be empty");
        } else if (municipality.isBlank()) {
            throw new IllegalArgumentException("Municipality can not be blank");
        }
        this.municipality = municipality;
    }

    /**
     * Sets the category.
     *
     * @param category the category
     */
    public void setCategory(char category) {
        if (!categories.contains(category)) {
            throw new IllegalArgumentException("Category is not a valid character");
        }
        this.category = category;
    }

    /**
     * Gets the postal code.
     *
     * @return the postal code
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * Gets city.
     *
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * Gets municipality code.
     *
     * @return the municipality code
     */
    public String getMunicipalityCode() {
        return municipalityCode;
    }

    /**
     * Gets municipality.
     *
     * @return the municipality
     */
    public String getMunicipality() {
        return municipality;
    }

    /**
     * Gets category.
     *
     * @return the category
     */
    public char getCategory() {
        return category;
    }
}