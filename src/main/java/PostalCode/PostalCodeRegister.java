package PostalCode;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * The type Postal code register.
 */
public class PostalCodeRegister {
    private final ArrayList<PostalCode> postalCodes;
    private List<PostalCode> searchList;

    /**
     * Instantiates a new Postal code register.
     */
    public PostalCodeRegister() {
        this.postalCodes = new ArrayList<>();
    }

    /**
     * Gets postal code list.
     *
     * @return the postal codes
     */
    public List<PostalCode> getPostalCodes() {
        return postalCodes;
    }

    /**
     * Add postal code.
     *
     * @param postalCode the postal code
     */
    public void addPostalCode(PostalCode postalCode) {
        this.postalCodes.add(postalCode);
    }

    /**
     * Postal code search method.
     *
     * @param postalCodeSearch the postal code search
     * @return the search result in a list
     */
    public List<PostalCode> searchPostalCode(String postalCodeSearch) {
        searchList = new ArrayList<>();
        if (postalCodeSearch == null) {
            throw new NullPointerException("Postal code search can not be empty");
        }
        searchList = this.postalCodes.stream().filter(postalCode -> postalCode.getPostalCode()
                .contains(postalCodeSearch)).collect(Collectors.toList());
        return searchList;
    }

    /**
     * City search method.
     *
     * @param citySearch the city search
     * @return the search result in a list
     */
    public List<PostalCode> searchCity(String citySearch) {
        searchList = new ArrayList<>();
        if (citySearch == null) {
            throw new NullPointerException("Postal code search can not be empty");
        }
        searchList = this.postalCodes.stream().filter(postalCode -> postalCode.getCity()
                .contains(citySearch.toUpperCase(Locale.ROOT))).collect(Collectors.toList());
        return searchList;
    }

    /**
     * Search for both city and postal code.
     *
     * @param postalCodeSearch the postal code search
     * @param citySearch       the city search
     * @return the search result in a list
     */
    public List<PostalCode> searchBoth(String postalCodeSearch, String citySearch) {
        searchList = new ArrayList<>();
        if (citySearch == null || postalCodeSearch == null) {
            throw new NullPointerException("Postal code and city search can not be empty");
        }
        searchList = this.postalCodes.stream().filter(postalCode -> postalCode.getCity()
                .contains(citySearch.toUpperCase(Locale.ROOT))).collect(Collectors.toList())
                .stream().filter(postalCode -> postalCode.getPostalCode()
                        .contains(postalCodeSearch)).collect(Collectors.toList());
        return searchList;
    }
}