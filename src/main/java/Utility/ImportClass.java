package Utility;

import PostalCode.PostalCode;
import PostalCode.PostalCodeRegister;
import org.apache.log4j.Logger;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * The type Import class.
 */
public class ImportClass {

    private static final Logger LOGGER = Logger.getLogger(ImportClass.class.getName());

    /**
     * Import postal codes.
     *
     * @param postalCodes the postal code register
     */
    public static void importPostalCodes(PostalCodeRegister postalCodes) {
        if (postalCodes == null) {
            throw new NullPointerException("Postal code register cannot be null");
        } else {
            try {
                ClassLoader classLoader = ImportClass.class.getClassLoader();
                InputStream is = classLoader.getResourceAsStream("Postnummerregister-ansi.txt");
                BufferedReader textReader = new BufferedReader(new InputStreamReader(is, StandardCharsets.ISO_8859_1));
                for (String row; (row = textReader.readLine()) != null; ) {
                    String[] data = row.split("\t");
                    postalCodes.addPostalCode(new PostalCode(data[0], data[1], data[2], data[3], data[4].charAt(0)));
                }
            } catch (IOException | NullPointerException e) {
                LOGGER.fatal("Import failed. " + e.getMessage());
            } catch (IllegalArgumentException iae) {
                LOGGER.warn("Postal code(s) with illegal argument in file. "+iae.getMessage());
            } catch (ArrayIndexOutOfBoundsException obe) {
                LOGGER.warn("Postal code(s) with too few fields in file. "+obe.getMessage());
            }
        }
    }
}
