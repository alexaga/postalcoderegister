package Utility;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import java.util.Optional;

// Taken from my second assignment
public class AlertClass {

    /**
     * The Alert.
     */
    private static Alert alert;

    /**
     * Create information alert.
     *
     * @param headerText  the header text
     * @param contentText the content text
     */
    public static void createInformationAlert(String headerText, String contentText) {
        alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(headerText);
        alert.setContentText(contentText);
        alert.show();
    }

    /**
     * Create confirmation alert with boolean confirmation return.
     *
     * @param headerText  the header text
     * @param contentText the content text
     * @return the boolean confirm
     */
    public static boolean createConfirmationAlert(String headerText, String contentText) {
        boolean confirm = false;
        alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setHeaderText(headerText);
        alert.setContentText(contentText);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            confirm = true;
        }
        return confirm;
    }
}
